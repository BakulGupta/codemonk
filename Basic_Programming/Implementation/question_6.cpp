#include<bits/stdc++.h>
using namespace std;
int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int t;
  cin>>t;
  while(t--)
  {
  int h;
  cin>>h;
  int temp=h-1;
  for(int i=1;i<=h;i++)
  {
      int k=temp;
      while(k--)
      {
       cout<<" ";
      }
      int el=2*i-1;
      while(el--)
      {
       cout<<"*";
      }
    cout<<endl;
    temp--;
  }
  }  
return 0;
}